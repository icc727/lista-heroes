class Heroe {
  final String nombre;
  final String poder;
  final String icon;
  final String color;

  Heroe._({this.nombre, this.poder, this.icon, this.color});

  factory Heroe.fromJson(Map<String, dynamic> json) {
    return new Heroe._(
        nombre: json['nombre'],
        poder: json['poder'],
        icon: json['icon'],
        color: json['color']);
  }
}
