import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lista/src/models/Heroe.dart';

class _HeroesProvider {
  final String _baseUrl = 'https://www.hectoralvarez.dev/icc727/';
  Map<String, String> _headers = {
    'Content-Type': 'application/json',
    'Authorization': 'mi\$up34Token'
  };
  List<dynamic> heroes = [];

  _HeroesProvider() {}

  Future<List<Heroe>> fetchHeroes() async {
    final response =
        await http.get(this._baseUrl + 'heroes.json', headers: this._headers);
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      List heroes = json['heroes'];
      return heroes.map((heroe) => new Heroe.fromJson(heroe)).toList();
      /* Map<String, dynamic> heroesMap = json.decode(response.body);
      heroes = heroesMap['heroes'];
      return heroes; */
    } else {
      throw Exception('Failed to load data');
    }
  }
}

final heroesProvider = new _HeroesProvider();
