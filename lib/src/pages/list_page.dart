import 'package:flutter/material.dart';
import 'package:lista/src/models/Heroe.dart';
import 'package:lista/src/services/heroes_provider.dart';
import 'package:lista/src/utils/json_icon_util.dart';

class ListPage extends StatefulWidget {
  ListPage({Key key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Listado de héroes"),
      ),
      body: _getHeroes(),
    );
  }

  Widget _getHeroes() {
    return FutureBuilder<List<Heroe>>(
      future: heroesProvider.fetchHeroes(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<Heroe>> snapshot) {
        List<Widget> lista = new List<Widget>();
        snapshot.data.forEach((heroe) {
          Column temp = Column(
            children: <Widget>[
              ListTile(
                title: Text(heroe.nombre),
                subtitle: Text(heroe.poder),
                leading: getIcon(heroe.icon, heroe.color),
              )
            ],
          );
          lista.add(temp);
        });
        return ListView(
          children: lista,
        );
      },
    );
  }
}
